import {Component, OnInit} from '@angular/core';
import {AuthService, RightEnum} from "../services/auth.service";

@Component({
  selector: 'app-simple-navbar',
  templateUrl: './simple-navbar.component.html',
  styleUrls: ['./simple-navbar.component.css']
})
export class SimpleNavbarComponent implements OnInit {

  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
  }

  logout()
  {
    this.authService.logout();
  }

  isLogged()
  {
    return this.authService.getUser;
  }

  hasAdmin()
  {
    return this.authService.getUser && this.authService.hasRight(RightEnum.ADMIN)
  }

  hasUser()
  {
    return this.authService.getUser && this.authService.hasRight(RightEnum.USER)
  }
}
