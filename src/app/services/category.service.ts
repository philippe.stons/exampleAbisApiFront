import { Injectable } from '@angular/core';
import {Crud} from "./crud";
import {Category} from "../models/category.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CategoryService extends Crud<Category> {

  constructor(
    protected httpC: HttpClient
  ) {
    super(httpC, {
      many: 'category',
      single: (id) => `category/${id}`
    })
  }
}
