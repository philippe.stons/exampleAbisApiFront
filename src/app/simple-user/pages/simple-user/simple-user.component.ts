import { Component, OnInit } from '@angular/core';
import {User} from "../../../models/user.model";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../../services/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {parseFormGroup} from "../../../shared/utils/utility-function";
import {Logger} from "../../../services/logger.service";
import {Address} from "../../../models/address.model";

@Component({
  selector: 'app-simple-user',
  templateUrl: './simple-user.component.html',
  styleUrls: ['./simple-user.component.css']
})
export class SimpleUserComponent implements OnInit {
  user: User;
  entityForm: FormGroup;
  edit: boolean = false;

  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.params['id'];
    this.edit = !isNaN(id);

    this.entityForm = this.fb.group({
      username: [null, [Validators.required, Validators.maxLength(255)]],
      password: [null, !this.edit ? [Validators.required] : null],
      // passwordConfirm: [null, [Validators.required]],
      email: [null, [Validators.required]],
      address: this.fb.group({
        city: [null, [Validators.required]],
        country: [null, [Validators.required]],
        number: [null, [Validators.required]],
        state: [null, [Validators.required]],
        street: [null, [Validators.required]],
        zipCode: [null, [Validators.required, Validators.min(0)]],
      })
    });

    if(this.user)
    {
      this.userService.find(id).subscribe((user) => {
        this.entityForm.patchValue(this.user);
      })
    } else {
      this.user = new User();
    }
  }

  getErrors(control: FormControl) {
    let errors: string[] = [];

    for (const errName in control.errors) {
      Logger.log(errName, control.errors[errName]);
      const error = control.errors[errName];

      switch (errName) {
        case 'required':
          errors.push('required');
          break;
        case 'min':
          errors.push(`minimum value : ${error.min}`);
          break;
        case 'max':
          errors.push(`maximum value : ${error.max}`);
          break;
        case 'maxLength':
          errors.push(`maximum length : ${error.max}`);
          break;
        default:
          errors.push(error);
      }
    }
  }

  get getAddressGroup(): FormGroup
  {
    return this.entityForm.get('address') as FormGroup;
  }

  submit()
  {
    Logger.log("SUBMIT FORM", this.entityForm);
    if(this.entityForm.invalid)
    {
      return;
    }

    let user = new User();
    user.username = this.entityForm.get('username').value;
    user.password = this.entityForm.get('password').value;
    user.email = this.entityForm.get('email').value;
    user.address = {} as Address;
    user.address.city = this.getAddressGroup.get('city').value;
    user.address.country = this.getAddressGroup.get('country').value;
    user.address.number = this.getAddressGroup.get('number').value;
    user.address.state = this.getAddressGroup.get('state').value;
    user.address.street = this.getAddressGroup.get('street').value;
    user.address.zipCode = this.getAddressGroup.get('zipCode').value;

    Logger.log('user', this.user);

    if(this.edit)
    {
      this.userService.update(user)
        .subscribe(() =>
        {
          this.router.navigate(['/user/', this.user.id, 'detail']);
        });
    } else
    {
      this.userService.insert(user)
        .subscribe(() =>
        {
          this.router.navigate(['/user']);
        });
    }
  }
}
