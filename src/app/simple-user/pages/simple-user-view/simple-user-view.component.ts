import { Component, OnInit } from '@angular/core';
import {User} from "../../../models/user.model";
import {UserService} from "../../../services/user.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-simple-user-view',
  templateUrl: './simple-user-view.component.html',
  styleUrls: ['./simple-user-view.component.css']
})
export class SimpleUserViewComponent implements OnInit {
  user: User;

  constructor(
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.params['id'];

    if(isNaN(id))
    {
      this.router.navigate(['/']);
    }

    this.userService.find(id)
      .subscribe((user) =>
      {
        this.user = user;
      });
  }

}
