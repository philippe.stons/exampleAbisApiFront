import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {User} from "../../../models/user.model";
import {MatPaginator} from "@angular/material/paginator";
import {UserService} from "../../../services/user.service";
import {AuthService, RightEnum} from "../../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-simple-user-list',
  templateUrl: './simple-user-list.component.html',
  styleUrls: ['./simple-user-list.component.css']
})
export class SimpleUserListComponent implements OnInit {
  displayColumns: string[] = ['Id', 'Username', 'Email', 'Roles', 'Actions'];
  dataSource = new MatTableDataSource<User>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.userService.findAll()
      .subscribe((users) =>
      {
        this.dataSource.data = users;
      });
  }

  hasUserRight()
  {
    return this.authService.hasRight(RightEnum.USER);
  }

  hasAdminRight()
  {
    return this.authService.hasRight(RightEnum.ADMIN);
  }

  createUser()
  {
    this.router.navigate(['/simple-user/add']);
  }

  editUser(id: number)
  {
    this.router.navigate(['/simple-user', id, 'edit']);
  }

  userDetails(id: number)
  {
    this.router.navigate(['/simple-user', id, 'detail']);
  }
}
