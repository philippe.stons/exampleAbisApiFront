import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from "../shared/shared.module";
import {UserRoutingModule} from "../user/user-routing.module";
import {AppTableModule} from "../shared/table/app-table.module";
import { SimpleUserComponent } from './pages/simple-user/simple-user.component';
import { SimpleUserListComponent } from './pages/simple-user-list/simple-user-list.component';
import { SimpleUserViewComponent } from './pages/simple-user-view/simple-user-view.component';
import {SimpleUserRoutingModule} from "./simple-user-routing.module";



@NgModule({
  declarations: [
    SimpleUserComponent,
    SimpleUserListComponent,
    SimpleUserViewComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SimpleUserRoutingModule,
    AppTableModule,
  ]
})
export class SimpleUserModule { }
