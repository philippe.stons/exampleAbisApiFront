import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {NotFoundComponent} from "./shared/not-fount/not-found.component";

const routes: Routes = [
  {
    path: 'user', loadChildren: () => import('./user/user.module')
      .then((m) => m.UserModule)
  },
  {
    path: 'simple-user', loadChildren: () => import('./simple-user/simple-user.module')
      .then((m) => m.SimpleUserModule)
  },
  { path: 'login', component: LoginComponent },

  { path: '**', component: NotFoundComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
