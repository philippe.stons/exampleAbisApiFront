export enum ToastrType {
  SUCCESS = 'success',
  WARNING = 'warning',
  ERROR = 'error'
}

export enum ToastrMessageType {
  CREATE = 'create',
  UPDATE = 'update',
  DELETE = 'delete',
  ERROR_400 = '400',
  ERROR_404 = '404',
  ERROR_500 = '500',
  ACCESS_DENIED = 'access_denied',
  DELETE_ERROR = 'delete_error',
}
