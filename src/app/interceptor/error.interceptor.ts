import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {catchError, Observable, throwError} from 'rxjs';
import {MatSnackBar} from "@angular/material/snack-bar";
import {ToastrMessageType, ToastrType} from "../models/toastr";
import {Logger} from "../services/logger.service";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(
    private snackBar: MatSnackBar,
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError((err) => {
      Logger.log('Exception', err);
      switch (err.status) {
        case 404:
          this.openMessage(ToastrMessageType.ERROR_400, 'Not found');
          break;
        case 400:
        case 401:
        case 403:
        case 500:
          this.openMessage(ToastrMessageType.ERROR_500, err.error.message);
          break;
      }

      return throwError(err);
    }));
  }

  openMessage(messageType: ToastrMessageType, customMessage: string)
  {
    let panelClass = '';
    switch (messageType) {
      case ToastrMessageType.ERROR_404:
        panelClass = 'toastr-warning';
        break;
      case ToastrMessageType.ERROR_500:
        panelClass = 'toastr-error';
        break;
      default:
        panelClass = 'toastr-success';
        break;
    }

    this.snackBar.open(customMessage, 'OK', {
      duration: 5000,
      panelClass,
      horizontalPosition: 'end',
      verticalPosition: 'top'
    });
  }
}
