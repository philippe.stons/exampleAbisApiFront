import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../../../services/user.service";
import {User} from "../../../models/user.model";
import {Logger} from "../../../services/logger.service";
import {Category} from "../../../models/category.model";
import {CategoryService} from "../../../services/category.service";
import {FormBuilder, FormControl} from "@angular/forms";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: User;
  edit: boolean = false;
  category: Category[];
  control: FormControl;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private categoryService: CategoryService,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.params['id'];
    Logger.log('edit user id', id);
    if(!isNaN(id))
    {
      this.edit = true;
      this.userService.find(id)
        .subscribe((user) => {
          this.user = user;
          Logger.log('find user', user);
        })
    }

    this.categoryService.findAll().subscribe((cats) => this.category = cats);
    this.control = new FormControl(null, null);
  }

  formChange()
  {
    let cat = this.control.value as string;

    console.log(this.category.filter((c) => c.id === parseInt(cat))[0]);
  }
}
